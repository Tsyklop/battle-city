package info.tsyklop.tanks.constant;

public class Constant {

    public static final String FXML_PATH = "fxml/";
    public static final String FXML_EXTENSION = ".fxml";

    public static final String LEVELS_PATH = "level/";

    public static final Integer GAME_FIELD_SIDE_LENGTH = 13;

    public static final Integer BLOCK_SIDE_LENGTH = 32;
    public static final Integer BLOCK_SIDE_HALF_LENGTH = BLOCK_SIDE_LENGTH / 2;
    public static final Integer BLOCK_SIDE_HALF_HALF_LENGTH = BLOCK_SIDE_HALF_LENGTH / 2;

}
