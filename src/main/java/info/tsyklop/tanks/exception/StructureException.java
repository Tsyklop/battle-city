package info.tsyklop.tanks.exception;

public class StructureException extends RuntimeException {

    public StructureException(String message) {
        super(buildMessage(message));
    }

    public StructureException(String message, Throwable cause) {
        super(buildMessage(message), cause);
    }

    public StructureException(Throwable cause) {
        super(cause);
    }

    public StructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(buildMessage(message), cause, enableSuppression, writableStackTrace);
    }

    private static String buildMessage(String message) {
        return "Level incorrect: " + message;
    }

}
