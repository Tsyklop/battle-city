package info.tsyklop.tanks.exception;

public class LevelNotFoundException extends RuntimeException {

    public LevelNotFoundException(String level) {
        super(buildMessage(level));
    }

    public LevelNotFoundException(String level, Throwable cause) {
        super(buildMessage(level), cause);
    }

    public LevelNotFoundException(Throwable cause) {
        super(cause);
    }

    public LevelNotFoundException(String level, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(buildMessage(level), cause, enableSuppression, writableStackTrace);
    }

    private static String buildMessage(String level) {
        return "Level " + level + " not found";
    }

}
