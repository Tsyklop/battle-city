package info.tsyklop.tanks.type;

public enum StateType {
    SPAWN, MOVING, STAYING, DESTROYING, DESTROYED;
}
