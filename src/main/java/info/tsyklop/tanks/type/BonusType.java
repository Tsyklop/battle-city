package info.tsyklop.tanks.type;

import info.tsyklop.tanks.utils.Utils;
import javafx.scene.image.Image;
import org.apache.commons.lang3.RandomUtils;

public enum BonusType {

    STAR("Star", Utils.loadImage("img/level/bonus/star.png"), 10000),
    TANK("Tank", Utils.loadImage("img/level/bonus/tank.png"), 10000),
    TIMER("Timer", Utils.loadImage("img/level/bonus/timer.png"), 10000),
    SHOVEL("Shovel", Utils.loadImage("img/level/bonus/shovel.png"), 10000),
    HELMET("Helmet", Utils.loadImage("img/level/bonus/helmet.png"), 10000),
    GRENADE("Grenade", Utils.loadImage("img/level/bonus/grenade.png"), 10000);

    private final String name;

    private final Image image;

    private final int duration;

    BonusType(String name, Image image, int duration) {
        this.name = name;
        this.image = image;
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public Image getImage() {
        return image;
    }

    public int getDuration() {
        return duration;
    }

    public static BonusType getRandomBonus() {
        return values()[RandomUtils.nextInt(0, values().length)];
    }

}
