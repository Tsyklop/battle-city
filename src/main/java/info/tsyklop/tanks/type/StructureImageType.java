package info.tsyklop.tanks.type;

import info.tsyklop.tanks.utils.Utils;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;

public enum StructureImageType {

    BRICK(Utils.loadImage("img/level/wall/brick.png")),
    STEEL(Utils.loadImage("img/level/wall/steel.png")),
    LEAVES(Utils.loadImage("img/level/trees.png")),
    EAGLE(Utils.loadImage("img/level/base/base.png")),
    EAGLE_DESTROYED(Utils.loadImage("img/level/base/base_destroyed.png")),
    ;

    private final Image image;

    private final Image[] parts;

    StructureImageType(Image image) {
        this.image = image;
        this.parts = divideIntoParts(image);
    }

    public Image getImage() {
        return image;
    }

    public Image[] getParts() {
        return parts;
    }

    private Image[] divideIntoParts(Image image) {

        int width = (int) (image.getWidth() / 2);
        int height = (int) (image.getHeight() / 2);

        Image[] parts = new Image[4];

        PixelReader pixelReader = image.getPixelReader();

        parts[0] = new WritableImage(pixelReader, 0, 0, width, height);
        parts[1] = new WritableImage(pixelReader, 8, 0, width, height);
        parts[2] = new WritableImage(pixelReader, 0, 8, width, height);
        parts[3] = new WritableImage(pixelReader, 8, 8, width, height);

        return parts;

    }

}
