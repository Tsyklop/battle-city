package info.tsyklop.tanks.type;

public enum  DirectionType {
    UP, RIGHT, DOWN, LEFT;
}
