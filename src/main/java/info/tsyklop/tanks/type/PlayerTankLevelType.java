package info.tsyklop.tanks.type;

import info.tsyklop.tanks.utils.Utils;
import javafx.scene.image.Image;

public enum PlayerTankLevelType {

    ONE_UP(1, new Image[]{Utils.loadImage("img/level/tank/player/tank_up_c0_t1.png"), Utils.loadImage("img/level/tank/player/tank_up_c0_t2.png")}, DirectionType.UP),
    ONE_RIGHT(1, new Image[]{Utils.loadImage("img/level/tank/player/tank_right_c0_t1.png"), Utils.loadImage("img/level/tank/player/tank_right_c0_t2.png")}, DirectionType.RIGHT),
    ONE_DOWN(1, new Image[]{Utils.loadImage("img/level/tank/player/tank_down_c0_t1.png"), Utils.loadImage("img/level/tank/player/tank_down_c0_t2.png")}, DirectionType.DOWN),
    ONE_LEFT(1, new Image[]{Utils.loadImage("img/level/tank/player/tank_left_c0_t1.png"), Utils.loadImage("img/level/tank/player/tank_left_c0_t2.png")}, DirectionType.LEFT),

    TWO_UP(2, new Image[]{Utils.loadImage("img/level/tank/player/tank_up_c0_t1_s1.png"), Utils.loadImage("img/level/tank/player/tank_up_c0_t2_s1.png")}, DirectionType.UP),
    TWO_RIGHT(2, new Image[]{Utils.loadImage("img/level/tank/player/tank_right_c0_t1_s1.png"), Utils.loadImage("img/level/tank/player/tank_right_c0_t2_s1.png")}, DirectionType.RIGHT),
    TWO_DOWN(2, new Image[]{Utils.loadImage("img/level/tank/player/tank_down_c0_t1_s1.png"), Utils.loadImage("img/level/tank/player/tank_down_c0_t2_s1.png")}, DirectionType.DOWN),
    TWO_LEFT(2, new Image[]{Utils.loadImage("img/level/tank/player/tank_left_c0_t1_s1.png"), Utils.loadImage("img/level/tank/player/tank_left_c0_t2_s1.png")}, DirectionType.LEFT),

    THREE_UP(3, new Image[]{Utils.loadImage("img/level/tank/player/tank_up_c0_t1_s2.png"), Utils.loadImage("img/level/tank/player/tank_up_c0_t2_s2.png")}, DirectionType.UP),
    THREE_RIGHT(3, new Image[]{Utils.loadImage("img/level/tank/player/tank_right_c0_t1_s2.png"), Utils.loadImage("img/level/tank/player/tank_right_c0_t2_s2.png")}, DirectionType.RIGHT),
    THREE_DOWN(3, new Image[]{Utils.loadImage("img/level/tank/player/tank_down_c0_t1_s2.png"), Utils.loadImage("img/level/tank/player/tank_down_c0_t2_s2.png")}, DirectionType.DOWN),
    THREE_LEFT(3, new Image[]{Utils.loadImage("img/level/tank/player/tank_left_c0_t1_s2.png"), Utils.loadImage("img/level/tank/player/tank_left_c0_t2_s2.png")}, DirectionType.LEFT),

    FOUR_UP(4, new Image[]{Utils.loadImage("img/level/tank/player/tank_up_c0_t1_s3.png"), Utils.loadImage("img/level/tank/player/tank_up_c0_t2_s3.png")}, DirectionType.UP),
    FOUR_RIGHT(4, new Image[]{Utils.loadImage("img/level/tank/player/tank_right_c0_t1_s3.png"), Utils.loadImage("img/level/tank/player/tank_right_c0_t2_s3.png")}, DirectionType.RIGHT),
    FOUR_DOWN(4, new Image[]{Utils.loadImage("img/level/tank/player/tank_down_c0_t1_s3.png"), Utils.loadImage("img/level/tank/player/tank_down_c0_t2_s3.png")}, DirectionType.DOWN),
    FOUR_LEFT(4, new Image[]{Utils.loadImage("img/level/tank/player/tank_left_c0_t1_s3.png"), Utils.loadImage("img/level/tank/player/tank_left_c0_t2_s3.png")}, DirectionType.LEFT);

    private int level;
    private Image[] images;
    private DirectionType direction;

    PlayerTankLevelType(int level, Image[] images, DirectionType direction) {
        this.level = level;
        this.images = images;
        this.direction = direction;
    }

    public int getLevel() {
        return level;
    }

    public Image[] getImages() {
        return images;
    }

    public DirectionType getDirection() {
        return direction;
    }

    public static int getMaxLevel() {
        return values()[values().length - 1].level;
    }

    public static PlayerTankLevelType findByLevelAndDirection(int level, DirectionType direction) {
        for (PlayerTankLevelType playerTankLevelType : values()) {
            if (playerTankLevelType.getLevel() == level && playerTankLevelType.getDirection().equals(direction)) {
                return playerTankLevelType;
            }
        }
        return null;
    }

}
