package info.tsyklop.tanks.type;

import java.util.Arrays;

public enum LevelStructureMarkerType {

    E(0, createFilledStructure()),
    L0(0, createFilledStructure()),
    ES(0, createFilledStructure()),
    PS(0, createFilledStructure()),
    B0(0, createFilledStructure()),

    B1(1, new int[][]{
            createFilledArray(), createEmptyArray(),
            createFilledArray(), createEmptyArray()
    }),

    B2(2, new int[][]{
            createEmptyArray(), createFilledArray(),
            createEmptyArray(), createFilledArray()
    }),

    B3(3, new int[][]{
            createFilledArray(), createFilledArray(),
            createEmptyArray(), createEmptyArray()
    }),

    B4(4, new int[][]{
            createEmptyArray(), createEmptyArray(),
            createFilledArray(), createFilledArray()
    }),

    B5(5, new int[][]{
            createFilledArray(), createEmptyArray(),
            createEmptyArray(), createEmptyArray()
    }),

    B6(6, new int[][]{
            createEmptyArray(), createFilledArray(),
            createEmptyArray(), createEmptyArray()
    }),

    B7(7, new int[][]{
            createEmptyArray(), createEmptyArray(),
            createFilledArray(), createEmptyArray()
    }),

    B8(8, new int[][]{
            createEmptyArray(), createEmptyArray(),
            createEmptyArray(), createFilledArray()
    }),

    S0(0, createFilledStructure()),

    S1(1, new int[][]{
            createFilledArray(), createEmptyArray(),
            createFilledArray(), createEmptyArray()
    }),

    S2(2, new int[][]{
            createEmptyArray(), createFilledArray(),
            createEmptyArray(), createFilledArray()
    }),

    S3(3, new int[][]{
            createFilledArray(), createFilledArray(),
            createEmptyArray(), createEmptyArray()
    }),

    S4(4, new int[][]{
            createEmptyArray(), createEmptyArray(),
            createFilledArray(), createFilledArray()
    }),

    S5(5, new int[][]{
            createFilledArray(), createEmptyArray(),
            createEmptyArray(), createEmptyArray()
    }),

    S6(6, new int[][]{
            createEmptyArray(), createFilledArray(),
            createEmptyArray(), createEmptyArray()
    }),

    S7(7, new int[][]{
            createEmptyArray(), createEmptyArray(),
            createFilledArray(), createEmptyArray()
    }),

    S8(8, new int[][]{
            createEmptyArray(), createEmptyArray(),
            createEmptyArray(), createFilledArray()
    });

    private final int index;

    private final int[][] structure;

    LevelStructureMarkerType(int index, int[][] structure) {
        this.index = index;
        this.structure = structure;
    }

    public int getIndex() {
        return this.index;
    }

    public int[][] getStructure() {
        return Arrays.stream(this.structure)
                .map(int[]::clone)
                .toArray(int[][]::new);
    }

    public static LevelStructureMarkerType get(String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException ignored) {
        }
        return null;
    }

    private static int[] createEmptyArray() {
        return new int[]{0, 0, 0, 0};
    }

    private static int[] createFilledArray() {
        return new int[]{1, 1, 1, 1};
    }

    private static int[][] createFilledStructure() {
        return new int[][]{
                createFilledArray(),
                createFilledArray(),
                createFilledArray(),
                createFilledArray()
        };
    }

}
