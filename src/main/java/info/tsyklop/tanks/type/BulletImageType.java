package info.tsyklop.tanks.type;

import info.tsyklop.tanks.utils.Utils;
import javafx.scene.image.Image;

public enum BulletImageType {

    BULLET_UP(Utils.loadImage("img/level/bullet/bullet_up.png")),
    BULLET_RIGHT(Utils.loadImage("img/level/bullet/bullet_right.png")),
    BULLET_DOWN(Utils.loadImage("img/level/bullet/bullet_down.png")),
    BULLET_LEFT(Utils.loadImage("img/level/bullet/bullet_left.png")),
    ;

    private final Image image;

    BulletImageType(Image image) {
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

}
