package info.tsyklop.tanks.window;

import info.tsyklop.tanks.controller.BaseController;
import info.tsyklop.tanks.controller.ModalController;
import info.tsyklop.tanks.utils.Utils;
import info.tsyklop.tanks.window.event.WindowLoadedEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import static info.tsyklop.tanks.constant.Constant.FXML_EXTENSION;
import static info.tsyklop.tanks.constant.Constant.FXML_PATH;
import static info.tsyklop.tanks.utils.Utils.exit;
import static info.tsyklop.tanks.utils.Utils.runThread;

public class Window {

    private Stage stage;

    private static volatile Window ourInstance;

    private static final Logger LOGGER = LoggerFactory.getLogger(Window.class);

    public static Window getInstance() {
        Window localInstance = ourInstance;
        if (localInstance == null) {
            synchronized (Window.class) {
                localInstance = ourInstance;
                if (localInstance == null) {
                    ourInstance = localInstance = new Window();
                }
            }
        }
        return localInstance;
    }

    public static Window getInstance(Stage stage) {
        Window localInstance = ourInstance;
        if (localInstance == null) {
            synchronized (Window.class) {
                localInstance = ourInstance;
                if (localInstance == null) {
                    ourInstance = localInstance = new Window(stage);
                }
            }
        }
        return localInstance;
    }

    private Window() {
        throw new IllegalArgumentException("Cannot create window object without stage");
    }

    private Window(Stage stage) {
        this.stage = stage;
        this.stage.setTitle("Tanks");
        this.stage.getIcons().add(new Image(Utils.getResourceAsStream("img/icon.png")));
    }

    public Stage getStage() {
        return stage;
    }

    public void show(String fxml) {
        show(fxml, null);
    }

    public void show(String fxml, WindowLoadedEvent windowLoadedEvent) {

        try {

            LOGGER.info(checkFxmlPath(fxml));

            FXMLLoader fxmlLoader = load(checkFxmlPath(fxml));

            if (fxmlLoader.getLocation() == null) {
                error("Window file not found");
                return;
            }

            Scene scene = new Scene(fxmlLoader.load());

            this.stage.hide();

            this.stage.setScene(scene);

            this.stage.setResizable(false);

            this.stage.show();

            Object controller = fxmlLoader.getController();

            if (controller instanceof BaseController) {
                BaseController baseController = (BaseController) controller;
                baseController.setStage(this.stage);
                baseController.onControllerLoaded();
            }

            if (windowLoadedEvent != null) {
                windowLoadedEvent.handle();
            }

        } catch (IOException e) {
            LOGGER.error("ERROR", e);
            error(e);
        }

    }

    public void showModal(String fxml) {
        showModal(fxml, false, null);
    }

    public void showModal(String fxml, boolean wait) {
        showModal(fxml, wait, null);
    }

    public void showModal(String fxml, Map<String, Object> data) {
        showModal(fxml, false, data);
    }

    public void showModal(String fxml, boolean wait, Map<String, Object> data) {

        try {

            LOGGER.info("SHOW MODAL");

            FXMLLoader fxmlLoader = load(checkFxmlPath(fxml));

            if (fxmlLoader.getLocation() == null) {
                error("Window file not found");
                return;
            }

            Stage dialog = new Stage();

            Scene scene = new Scene(fxmlLoader.load());

            Object controller = fxmlLoader.getController();

            if (controller instanceof ModalController) {
                ModalController modalController = (ModalController) controller;
                modalController.setData(data);
                modalController.setStage(dialog);
                modalController.modalInitialized();
            }

            dialog.setScene(scene);

            dialog.initOwner(this.stage.isShowing() ? this.stage : null);
            dialog.initModality(this.stage.isShowing() ? Modality.APPLICATION_MODAL : Modality.WINDOW_MODAL);

            dialog.setResizable(false);

            if (wait) {
                dialog.showAndWait();
            } else {
                dialog.show();
            }

        } catch (IOException e) {
            LOGGER.error("ERROR", e);
            error("Opening window error");
        }

    }


    public void error(Throwable t) {

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fatal error!");
        alert.setHeaderText("Tanks Fatal error! Please contact with administrator.");
        alert.setContentText("Error message: " + t.getMessage());

        StringWriter sw = new StringWriter();

        PrintWriter pw = new PrintWriter(sw);

        t.printStackTrace(pw);

        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();

    }

    public void error(String message) {
        error(message, false);
    }

    public void error(final String message, final boolean fatal) {

        runThread(() -> {

            Alert alert = new Alert(Alert.AlertType.ERROR);

            alert.setTitle("Error");
            alert.setHeaderText(message);
            alert.setContentText(null);

            alert.showAndWait();

            if (fatal) {
                exit();
            }

        });

    }

    public void success(final String message) {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Successfully!");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();

    }

    private String checkFxmlPath(String fxml) {

        if (!fxml.startsWith(FXML_PATH)) {
            fxml = FXML_PATH + fxml;
        }

        if (!fxml.endsWith(FXML_EXTENSION)) {
            fxml = fxml + FXML_EXTENSION;
        }

        return fxml;
    }

    private FXMLLoader load(String fxml) {
        return new FXMLLoader(Utils.getResourceURL(fxml));
    }


}
