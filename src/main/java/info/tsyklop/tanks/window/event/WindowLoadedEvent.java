package info.tsyklop.tanks.window.event;

@FunctionalInterface
public interface WindowLoadedEvent {
    void handle();
}
