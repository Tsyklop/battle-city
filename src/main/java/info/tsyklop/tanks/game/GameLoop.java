package info.tsyklop.tanks.game;

import info.tsyklop.tanks.type.DirectionType;
import info.tsyklop.tanks.world.World;
import info.tsyklop.tanks.world.entity.impl.movable.PlayerTankEntity;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

public class GameLoop extends AnimationTimer {

    private final World world;

    private final GraphicsContext graphicsContext;

    private final long startNanoTime = System.nanoTime();

    public GameLoop(World world, GraphicsContext graphicsContext) {
        this.world = world;
        this.graphicsContext = graphicsContext;
    }

    @Override
    public void handle(long now) {
        handleKeys();
        this.world.render(this.graphicsContext, (now - this.startNanoTime) / 1000000000.0);
    }

    private void handleKeys() {

        PlayerTankEntity playerTank = this.world.getCurrentLevel().getPlayerTank();

        switch (this.world.getDirectionKey()) {
            case W:
            case UP:
                playerTank.move(DirectionType.UP);
                break;
            case D:
            case RIGHT:
                playerTank.move(DirectionType.RIGHT);
                break;
            case S:
            case DOWN:
                playerTank.move(DirectionType.DOWN);
                break;
            case A:
            case LEFT:
                playerTank.move(DirectionType.LEFT);
                break;
            default:
                if (!playerTank.isSpawning()) {
                    this.world.getCurrentLevel().getPlayerTank().stay();
                }
                break;
        }

        if (playerTank.isWasShot()
                && !playerTank.getBullet().isDestroy()
                && !playerTank.getBullet().isDestroyed()) {
            playerTank.getBullet().move();
        }

        for (KeyCode keyCode : this.world.getKeys()) {
            switch (keyCode) {
                case SPACE:
                    playerTank.fire();
                    break;
            }
        }

    }

}
