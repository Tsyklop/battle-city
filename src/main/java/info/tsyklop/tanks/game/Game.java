package info.tsyklop.tanks.game;

import info.tsyklop.tanks.world.World;
import javafx.scene.canvas.GraphicsContext;

import java.io.IOException;
import java.util.Objects;

public class Game {

    private World world;

    private GameLoop gameLoop;

    private boolean started = false;

    private static volatile Game instance;

    public static Game getInstance() {
        if (instance == null) {
            synchronized (Game.class) {
                if (instance == null) {
                    instance = new Game();
                }
            }
        }
        return instance;
    }

    public boolean start(GraphicsContext gc) throws IOException {
        Objects.requireNonNull(gc);

        if(!this.started) {

            this.world = new World();

            if(this.world.loadNextLevel()) {

                this.gameLoop = new GameLoop(this.world, gc);
                this.gameLoop.start();

                this.started = true;

            }

        }

        return this.started;

    }

    public boolean stop() {

        if(this.started) {
            this.gameLoop.stop();
            this.world.resetLevel();
            this.world = null;
            this.started = false;
        }

        return !this.started;
    }

    public boolean isStarted() {
        return started;
    }

    public World getWorld() {
        return world;
    }

}
