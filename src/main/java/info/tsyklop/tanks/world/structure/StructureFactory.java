package info.tsyklop.tanks.world.structure;

import info.tsyklop.tanks.exception.StructureException;
import info.tsyklop.tanks.type.LevelStructureMarkerType;
import info.tsyklop.tanks.world.entity.impl.structure.BrickEntity;
import info.tsyklop.tanks.world.entity.impl.structure.EagleEntity;
import info.tsyklop.tanks.world.entity.impl.structure.LeavesEntity;
import info.tsyklop.tanks.world.entity.impl.structure.SteelEntity;
import info.tsyklop.tanks.world.entity.impl.structure.spawn.EnemySpawnEntity;
import info.tsyklop.tanks.world.entity.impl.structure.spawn.PlayerSpawnEntity;

import java.util.Objects;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

public class StructureFactory {

    public Structure load(String content) {

        Structure structure = new Structure();

        String[] lines = content.replaceAll("\r", "").split("\n");

        for (int i = 0; i < lines.length; i++) {

            String[] elements = lines[i].split(" ");

            for (int j = 0; j < elements.length; j++) {

                LevelStructureMarkerType element = LevelStructureMarkerType.get(elements[j]);

                if (element != null) {
                    switch (element) {
                        case B0:
                        case B1:
                        case B2:
                        case B3:
                        case B4:
                        case B5:
                        case B6:
                        case B7:
                        case B8:
                            structure.addStructure(new BrickEntity(BLOCK_SIDE_LENGTH * j, BLOCK_SIDE_LENGTH * i, element));
                            break;
                        case L0:
                            structure.addStructure(new LeavesEntity(BLOCK_SIDE_LENGTH * j, BLOCK_SIDE_LENGTH * i, element));
                            break;
                        case S0:
                        case S1:
                        case S2:
                        case S3:
                        case S4:
                        case S5:
                        case S6:
                        case S7:
                        case S8:
                            structure.addStructure(new SteelEntity(BLOCK_SIDE_LENGTH * j, BLOCK_SIDE_LENGTH * i, element));
                            break;
                        case E:
                            structure.addStructure(new EagleEntity(BLOCK_SIDE_LENGTH * j, BLOCK_SIDE_LENGTH * i, element));
                            break;
                        case ES:
                            structure.addEnemySpawn(new EnemySpawnEntity(BLOCK_SIDE_LENGTH * j, BLOCK_SIDE_LENGTH * i, element));
                            break;
                        case PS:
                            structure.setPlayerSpawn(new PlayerSpawnEntity(BLOCK_SIDE_LENGTH * j, BLOCK_SIDE_LENGTH * i, element));
                            break;
                    }
                }

            }

        }

        if (structure.getStructureEntities().isEmpty()) {
            throw new StructureException("Structures not found");
        }

        if (structure.getEnemySpawnEntities().isEmpty()) {
            throw new StructureException("Enemies Spawn(s) not found");
        }

        if (Objects.isNull(structure.getPlayerSpawnEntity())) {
            throw new StructureException("Player Spawn not found");
        }

        return structure;

    }

}
