package info.tsyklop.tanks.world.structure;

import info.tsyklop.tanks.world.entity.StructureEntity;
import info.tsyklop.tanks.world.entity.impl.structure.spawn.EnemySpawnEntity;
import info.tsyklop.tanks.world.entity.impl.structure.spawn.PlayerSpawnEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Structure {

    private PlayerSpawnEntity playerSpawnEntity;

    private final List<StructureEntity> structureEntities = new ArrayList<>();

    private final List<EnemySpawnEntity> enemySpawnEntities = new ArrayList<>();

    Structure() {}

    public PlayerSpawnEntity getPlayerSpawnEntity() {
        return playerSpawnEntity;
    }

    public List<StructureEntity> getStructureEntities() {
        return structureEntities;
    }

    public List<EnemySpawnEntity> getEnemySpawnEntities() {
        return enemySpawnEntities;
    }

    public void setPlayerSpawn(PlayerSpawnEntity playerSpawn) {
        Objects.requireNonNull(playerSpawn, "Player spawn cannot be null");
        this.playerSpawnEntity = playerSpawn;
    }

    public void addStructure(StructureEntity structureEntity) {
        Objects.requireNonNull(structureEntity, "Enemy spawn cannot be null");
        this.structureEntities.add(structureEntity);
    }

    public void addEnemySpawn(EnemySpawnEntity enemySpawnEntity) {
        Objects.requireNonNull(enemySpawnEntity, "Enemy spawn cannot be null");
        this.enemySpawnEntities.add(enemySpawnEntity);
    }

}
