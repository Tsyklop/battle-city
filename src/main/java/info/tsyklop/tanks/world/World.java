package info.tsyklop.tanks.world;

import info.tsyklop.tanks.world.entity.GraphicEntity;
import info.tsyklop.tanks.world.entity.WorldEntity;
import info.tsyklop.tanks.world.entity.impl.movable.BulletEntity;
import info.tsyklop.tanks.world.entity.impl.movable.PlayerTankEntity;
import info.tsyklop.tanks.world.level.Level;
import info.tsyklop.tanks.world.level.LevelFactory;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import lombok.Data;

import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

public class World implements GraphicEntity {

    private Level currentLevel;

    private KeyCode directionKey = KeyCode.UNDEFINED;

    private final Set<KeyCode> keys = new HashSet<>();

    private final Object directionKeyLock = new Object();

    private final LevelFactory levelFactory = new LevelFactory();

    private final Rectangle2D worldRectangle = new Rectangle2D(0, 0, BLOCK_SIDE_LENGTH * 13, BLOCK_SIDE_LENGTH * 13);

    public World() throws IOException {
    }

    @Override
    public void render(GraphicsContext gc, double time) {
        gc.setFill(Color.BLACK);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        this.currentLevel.render(gc, time);
    }

    public KeyCode getDirectionKey() {
        synchronized (this.directionKeyLock) {
            return this.directionKey;
        }
    }

    public void setDirectionKey(KeyCode directionKey) {
        synchronized (directionKeyLock) {
            this.directionKey = directionKey;
        }
    }

    public synchronized void addKey(KeyCode keyCode) {
        if (isControlKey(keyCode)) {
            setDirectionKey(keyCode);
        } else {
            this.keys.add(keyCode);
        }
    }

    public synchronized void removeKey(KeyCode keyCode) {
        if (isControlKey(keyCode)) {
            if (this.directionKey == keyCode) {
                setDirectionKey(KeyCode.UNDEFINED);
            }
        } else {
            this.keys.remove(keyCode);
        }
    }

    public synchronized Set<KeyCode> getKeys() {
        return this.keys;
    }

    public void resetLevel() {
        this.currentLevel = null;
    }

    public boolean loadNextLevel() {

        if (Objects.isNull(this.currentLevel)) {
            this.currentLevel = this.levelFactory.load(this, 1);
        } else {
            this.currentLevel = this.levelFactory.load(this,this.currentLevel.getIndex() + 1);
        }

        return Objects.nonNull(this.currentLevel);

    }

    private boolean isControlKey(KeyCode keyCode) {
        return KeyCode.UP == keyCode || KeyCode.DOWN == keyCode || KeyCode.RIGHT == keyCode || KeyCode.LEFT == keyCode
                || KeyCode.W == keyCode || KeyCode.S == keyCode || KeyCode.D == keyCode || KeyCode.A == keyCode;
    }

    public Level getCurrentLevel() {
        return this.currentLevel;
    }

    public boolean containsEntity(WorldEntity worldEntity) {
        return this.worldRectangle.contains(worldEntity.getRectangle());
    }

}
