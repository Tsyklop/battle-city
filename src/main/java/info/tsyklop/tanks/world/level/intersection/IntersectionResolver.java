package info.tsyklop.tanks.world.level.intersection;

import info.tsyklop.tanks.world.World;

public interface IntersectionResolver<T1, T2> {

    World getWorld();

    boolean resolve(T1 t1, T2 t2);

}
