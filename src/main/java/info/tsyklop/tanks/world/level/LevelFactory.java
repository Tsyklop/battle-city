package info.tsyklop.tanks.world.level;

import info.tsyklop.tanks.world.World;
import info.tsyklop.tanks.world.structure.StructureFactory;
import info.tsyklop.tanks.utils.Utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static info.tsyklop.tanks.constant.Constant.LEVELS_PATH;
import static info.tsyklop.tanks.utils.Utils.getResourceFiles;

public class LevelFactory {

    private final Map<Integer, String> levelsContents = new HashMap<>();

    private final StructureFactory structureFactory = new StructureFactory();

    public LevelFactory() throws IOException {
        loadLevels();
    }

    public Level load(World world, Integer number) {
        if(this.levelsContents.containsKey(number)) {
            return new Level(number, world, this.structureFactory.load(this.levelsContents.get(number)));
        }
        return null;
    }

    private void loadLevels() throws IOException {

        List<String> levelsFilesNames = getResourceFiles(LEVELS_PATH, ".level");

        int index = 1;
        for (String levelFileName : levelsFilesNames) {
            this.levelsContents.put(index, Utils.getResourceToString(LEVELS_PATH + levelFileName));
            index++;
        }

    }

}
