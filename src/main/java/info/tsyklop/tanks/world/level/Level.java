package info.tsyklop.tanks.world.level;

import info.tsyklop.tanks.type.BonusType;
import info.tsyklop.tanks.world.World;
import info.tsyklop.tanks.world.entity.GraphicEntity;
import info.tsyklop.tanks.world.entity.StructureEntity;
import info.tsyklop.tanks.world.entity.WorldEntity;
import info.tsyklop.tanks.world.entity.impl.movable.BulletEntity;
import info.tsyklop.tanks.world.entity.impl.movable.EnemyTankEntity;
import info.tsyklop.tanks.world.entity.impl.movable.PlayerTankEntity;
import info.tsyklop.tanks.world.entity.impl.structure.BonusEntity;
import info.tsyklop.tanks.world.level.intersection.IntersectionResolver;
import info.tsyklop.tanks.world.structure.Structure;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import lombok.Data;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

@Data
public class Level implements GraphicEntity {

    private int index;

    private int lives = 3;

    private int enemies = 20;

    private boolean loaded = false;

    private final World world;

    private final Structure structure;

    private PlayerTankEntity playerTank;

    private final List<BonusEntity> bonuses = new ArrayList<>();

    private final List<BulletEntity> bullets = new ArrayList<>();

    private final List<EnemyTankEntity> enemiesTanks = new ArrayList<>();

    private final List<IntersectionResolver> intersectionResolvers = new ArrayList<>();

    Level(int index, World world, Structure structure) {
        Objects.requireNonNull(structure, "Structure content can not be null");
        this.index = index;
        this.world = world;
        this.structure = structure;
        this.playerTank = new PlayerTankEntity(
                structure.getPlayerSpawnEntity().getPoint().getX(),
                structure.getPlayerSpawnEntity().getPoint().getY()
        );
    }

    @Override
    public void render(final GraphicsContext gc, final double time) {
        Objects.requireNonNull(gc, "GraphicsContext can not be null");

        //generateBonuses(gc, time);

        resolveWorldIntersections();

        resolveBulletsIntersections();

        resolveEntitiesIntersections();

        renderEntities(gc, time);

        this.playerTank.render(gc, time);

    }

    private void renderEntities(GraphicsContext gc, double time) {

        for (StructureEntity structureEntity : this.structure.getStructureEntities()) {
            structureEntity.render(gc, time);
        }

        for (EnemyTankEntity enemyTankEntity : this.enemiesTanks) {
            enemyTankEntity.render(gc, time);
        }

        for (BonusEntity bonusEntity : this.bonuses) {
            bonusEntity.render(gc, time);
        }

        if(this.playerTank.isWasShot()) {
            this.playerTank.getBullet().render(gc, time);
        }

    }

    private void resolveWorldIntersections() {
        if(!this.world.containsEntity(this.playerTank)) {
            this.playerTank.revertPoint();
        }
    }

    private void resolveBulletsIntersections() {

        BulletEntity playerBullet = this.playerTank.getBullet();

        if (Objects.nonNull(playerBullet)) {

            if(playerBullet.isDestroyed()) {
                playerTank.resetBullet();
            } else {

                if(!this.world.containsEntity(playerBullet)) {
                    playerBullet.revertPoint();
                    this.playerTank.resetBullet();
                }

                for (StructureEntity structureEntity : this.structure.getStructureEntities()) {
                    if (structureEntity.isStructure() && isIntersects(structureEntity, playerBullet)) {
                        structureEntity.destruction(playerBullet);
                        playerBullet.destroy();
                        break;
                    }
                }

                for (EnemyTankEntity enemyTankEntity : enemiesTanks) {
                    if (isIntersects(enemyTankEntity, playerBullet)) {
                        enemyTankEntity.destroy();
                    }
                }

            }

        }

    }

    private void resolveEntitiesIntersections() {

        for (StructureEntity structureEntity : this.structure.getStructureEntities()) {
            if (structureEntity.isStructure() && isIntersects(structureEntity, this.playerTank)) {
                this.playerTank.revertPoint();
            }
        }

        for (EnemyTankEntity enemyTankEntity : enemiesTanks) {
            if (isIntersects(enemyTankEntity, this.playerTank)) {
                enemyTankEntity.revertPoint();
                this.playerTank.revertPoint();
            }
        }

        for (BonusEntity bonusEntity : bonuses) {
            if (bonusEntity.getRectangle().intersects(this.playerTank.getRectangle())) {
                // TODO bonus reward
            }
        }

    }

    private void generateBonuses(GraphicsContext gc, double time) {
        if (time > 1 && ((int) time) % RandomUtils.nextInt(1, 101) == 0) {

            double x = RandomUtils.nextDouble(0, gc.getCanvas().getWidth() - BLOCK_SIDE_LENGTH + 1);
            double y = RandomUtils.nextDouble(0, gc.getCanvas().getHeight() - BLOCK_SIDE_LENGTH + 1);

            BonusEntity bonusEntity = new BonusEntity(x, y, BonusType.getRandomBonus());

            if (this.bonuses.stream().noneMatch(be -> be.equals(bonusEntity))) {
                this.bonuses.add(bonusEntity);
            }

        }
    }

    private boolean isIntersects(WorldEntity firstEntity, WorldEntity secondEntity) {
        return firstEntity.getRectangle().intersects(secondEntity.getRectangle());
    }

}
