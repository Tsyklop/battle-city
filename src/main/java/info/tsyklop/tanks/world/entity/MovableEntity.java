package info.tsyklop.tanks.world.entity;

import info.tsyklop.tanks.type.DirectionType;
import info.tsyklop.tanks.type.StateType;
import info.tsyklop.tanks.utils.Utils;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

import java.util.Objects;

public abstract class MovableEntity extends WorldEntity {

    private int movingStep = 2;

    private Point2D previousPoint;

    private double duration = 0.100;

    private int appearRenderCount = 0;

    private StateType state = StateType.SPAWN;

    private DirectionType direction = DirectionType.UP;

    private static final Image[] APPEARS = new Image[4];

    static {
        APPEARS[0] = Utils.loadImage("img/level/appear/appear_1.png");
        APPEARS[1] = Utils.loadImage("img/level/appear/appear_2.png");
        APPEARS[2] = Utils.loadImage("img/level/appear/appear_3.png");
        APPEARS[3] = Utils.loadImage("img/level/appear/appear_4.png");
    }

    public MovableEntity(double x, double y) {
        super(x, y);
    }

    public MovableEntity(double x, double y, int movingStep) {
        super(x, y);
        this.movingStep = movingStep;
    }

    public MovableEntity(double x, double y, DirectionType direction) {
        super(x, y);
        this.direction = direction;
    }

    public MovableEntity(double x, double y, int movingStep, DirectionType direction) {
        this(x, y, movingStep);
        this.direction = direction;
    }

    @Override
    public void setPoint(Point2D point) {
        this.previousPoint = getPoint();
        super.setPoint(point);
    }

    @Override
    public void setRectangle(double x, double y, double width, double height) {
        super.setRectangle(x + 2, y + 2, width - 4, height - 4);
    }

    public void stay() {
        setState(StateType.STAYING);
    }

    public void destroy() {
        setState(StateType.DESTROYING);
    }

    public void destroyed() {
        setState(StateType.DESTROYED);
    }

    public void revertPoint() {
        if(Objects.nonNull(this.previousPoint)) {
            setPoint(this.previousPoint);
        }
    }

    public void move(DirectionType direction) {
        Objects.requireNonNull(direction, "Direction cannot be null");

        setDirection(direction);
        setState(StateType.MOVING);

        switch (direction) {
            case UP:
                setPoint(getPoint().add(0, -getMovingStep()));
                break;
            case RIGHT:
                setPoint(getPoint().add(getMovingStep(), 0));
                break;
            case DOWN:
                setPoint(getPoint().add(0, getMovingStep()));
                break;
            case LEFT:
                setPoint(getPoint().add(-getMovingStep(), 0));
                break;
        }

    }

    public boolean isMoving() {
        return this.state == StateType.MOVING;
    }

    public boolean isSpawning() {
        return this.state == StateType.SPAWN;
    }

    public boolean isDestroy() {
        return this.state == StateType.DESTROYING;
    }

    public boolean isDestroyed() {
        return this.state == StateType.DESTROYED;
    }

    public int getMovingStep() {
        return this.movingStep;
    }

    public StateType getState() {
        return this.state;
    }

    public void setState(StateType state) {
        this.state = state;
    }

    public double getDuration() {
        return this.duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public DirectionType getDirection() {
        return direction;
    }

    public void setDirection(DirectionType direction) {
        this.direction = direction;
    }

    protected Image getAppear(final double time) {
        appearRenderCount++;
        return APPEARS[(int) ((time % (APPEARS.length * duration)) / duration)];
    }

    protected boolean isAppearFinished() {
        return appearRenderCount >= APPEARS.length * 12;
    }

    protected abstract Image resolveImageByDirection(final double time);

}
