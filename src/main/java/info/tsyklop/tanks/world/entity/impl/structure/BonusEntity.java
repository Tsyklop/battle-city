package info.tsyklop.tanks.world.entity.impl.structure;

import info.tsyklop.tanks.type.BonusType;
import info.tsyklop.tanks.type.StructureImageType;
import info.tsyklop.tanks.world.entity.StructureEntity;
import javafx.scene.canvas.GraphicsContext;

import java.util.Objects;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

public class BonusEntity extends StructureEntity {

    private final BonusType bonusType;

    public BonusEntity(double x, double y, BonusType bonusType) {
        super(x, y, null);
        this.bonusType = bonusType;
    }

    @Override
    public boolean isBonus() {
        return true;
    }

    @Override
    public boolean isDestructible() {
        return false;
    }

    @Override
    public void render(GraphicsContext gc, final double time) {

        gc.drawImage(bonusType.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);

        /*switch (bonusType) {
            case STAR:
                gc.drawImage(bonusType.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
                break;
            case TANK:

                break;
            case TIMER:

                break;
            case SHOVEL:

                break;
            case HELMET:

                break;
            case GRENADE:

                break;
        }*/

    }

    public BonusType getBonusType() {
        return bonusType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BonusEntity that = (BonusEntity) o;
        return bonusType == that.bonusType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bonusType);
    }
}
