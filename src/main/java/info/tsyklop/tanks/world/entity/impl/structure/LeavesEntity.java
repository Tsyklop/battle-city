package info.tsyklop.tanks.world.entity.impl.structure;

import info.tsyklop.tanks.type.LevelStructureMarkerType;
import info.tsyklop.tanks.type.StructureImageType;
import info.tsyklop.tanks.world.entity.StructureEntity;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import static info.tsyklop.tanks.constant.Constant.*;

public class LeavesEntity extends StructureEntity {

    public LeavesEntity(double x, double y, LevelStructureMarkerType markerType) {
        super(x, y, markerType);
    }

    @Override
    public boolean isDestructible() {
        return false;
    }

    @Override
    public void render(GraphicsContext gc, final double time) {
        gc.setFill(Color.BROWN);
        gc.drawImage(StructureImageType.LEAVES.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
    }

}
