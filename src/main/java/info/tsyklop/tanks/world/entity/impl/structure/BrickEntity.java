package info.tsyklop.tanks.world.entity.impl.structure;

import info.tsyklop.tanks.type.LevelStructureMarkerType;
import info.tsyklop.tanks.type.StructureImageType;
import info.tsyklop.tanks.world.entity.StructureEntity;
import javafx.scene.canvas.GraphicsContext;

public class BrickEntity extends StructureEntity {

    public BrickEntity(double x, double y, LevelStructureMarkerType element) {
        super(x, y, element, StructureImageType.BRICK);
    }

}
