package info.tsyklop.tanks.world.entity.impl.movable;

import info.tsyklop.tanks.type.BonusType;
import info.tsyklop.tanks.type.PlayerTankLevelType;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PlayerTankEntity extends TankEntity {

    private List<BonusType> bonuses = new ArrayList<>();

    public PlayerTankEntity(double x, double y) {
        super(x, y);
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    public void fire() {
        if(isSpawning() || isDestroy() || Objects.nonNull(this.bullet)) {
            return;
        }
        this.bullet = new BulletEntity(getPoint().getX(), getPoint().getY(), getDirection(), this);
    }

    @Override
    public void upgrade() {
        if(this.level < PlayerTankLevelType.getMaxLevel()) {
            this.level++;
        }
    }

    @Override
    protected Image resolveImageByDirection(final double time) {

        PlayerTankLevelType playerTankLevelType = PlayerTankLevelType.findByLevelAndDirection(level, getDirection());

        if (playerTankLevelType != null) {

            Image[] images = playerTankLevelType.getImages();

            switch (getState()) {
                case SPAWN:
                    return getAppear(time);
                case DESTROYING:
                    return null;
                case MOVING:
                    return images[(int) ((time % (images.length * getDuration())) / getDuration())];
                case STAYING:
                default:
                    return images[0];
            }

        }

        return null;

    }

}
