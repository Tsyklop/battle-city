package info.tsyklop.tanks.world.entity.impl.structure;

import info.tsyklop.tanks.type.LevelStructureMarkerType;
import info.tsyklop.tanks.type.StructureImageType;
import info.tsyklop.tanks.world.entity.StructureEntity;
import javafx.scene.canvas.GraphicsContext;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

public class EagleEntity extends StructureEntity {

    public EagleEntity(double x, double y, LevelStructureMarkerType markerType) {
        super(x, y, markerType);
    }

    @Override
    public void render(GraphicsContext gc, final double time) {
        switch (getMarkerType().getIndex()) {
            case 0:
                gc.drawImage(StructureImageType.EAGLE.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
                break;
            case 1:
                gc.drawImage(StructureImageType.EAGLE_DESTROYED.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
                break;
        }
    }

}
