package info.tsyklop.tanks.world.entity.impl.movable;

import info.tsyklop.tanks.world.entity.MovableEntity;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import info.tsyklop.tanks.type.DirectionType;
import info.tsyklop.tanks.type.StateType;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

public abstract class TankEntity extends MovableEntity {

    protected int level = 1;

    protected boolean shield = false;

    protected BulletEntity bullet;

    protected TankEntity(double x, double y) {
        super(x, y);
    }

    /*@Override
    public void move(DirectionType direction) {
        Objects.requireNonNull(direction, "Direction cannot be null");

        setDirection(direction);
        setState(StateType.MOVING);

        switch (direction) {
            case UP:
                setPoint(getPoint().add(0, -MOVING_STEP));
                break;
            case RIGHT:
                setPoint(getPoint().add(MOVING_STEP, 0));
                break;
            case DOWN:
                setPoint(getPoint().add(0, MOVING_STEP));
                break;
            case LEFT:
                setPoint(getPoint().add(-MOVING_STEP, 0));
                break;
        }

    }*/

    @Override
    public void render(GraphicsContext gc, final double time) {
        if(isAppearFinished() && isSpawning()) {
            this.stay();
        }
        gc.drawImage(resolveImageByDirection(time), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
    }

    public boolean isShieldActive() {
        return shield;
    }

    public boolean isWasShot() {
        return Objects.nonNull(this.bullet);
    }

    public void resetBullet() {
        this.bullet = null;
    }

    public BulletEntity getBullet() {
        return this.bullet;
    }

    public abstract void upgrade();

    public abstract boolean isPlayer();

}
