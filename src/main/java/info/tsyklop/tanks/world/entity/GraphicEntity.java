package info.tsyklop.tanks.world.entity;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;

public interface GraphicEntity {
    void render(final GraphicsContext gc, final double time);
}
