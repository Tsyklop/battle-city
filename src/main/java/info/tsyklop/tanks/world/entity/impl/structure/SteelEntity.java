package info.tsyklop.tanks.world.entity.impl.structure;

import info.tsyklop.tanks.type.LevelStructureMarkerType;
import info.tsyklop.tanks.type.StructureImageType;
import info.tsyklop.tanks.world.entity.StructureEntity;
import javafx.scene.canvas.GraphicsContext;

public class SteelEntity extends StructureEntity {

    public SteelEntity(double x, double y, LevelStructureMarkerType markerType) {
        super(x, y, markerType, StructureImageType.STEEL);
    }

    @Override
    public boolean isDestructible() {
        return false;
    }

}
