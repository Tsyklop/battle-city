package info.tsyklop.tanks.world.entity.impl.movable;

import info.tsyklop.tanks.type.BulletImageType;
import info.tsyklop.tanks.type.DirectionType;
import info.tsyklop.tanks.type.StateType;
import info.tsyklop.tanks.utils.Utils;
import info.tsyklop.tanks.world.entity.MovableEntity;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

public class BulletEntity extends MovableEntity {

    private TankEntity owner;

    private int explosionImageCount = 0;

    private static final Image[] EXPLOSION = new Image[3];

    static {
        EXPLOSION[0] = Utils.loadImage("img/level/bullet/explosion/bullet_explosion_1.png");
        EXPLOSION[1] = Utils.loadImage("img/level/bullet/explosion/bullet_explosion_2.png");
        EXPLOSION[2] = Utils.loadImage("img/level/bullet/explosion/bullet_explosion_3.png");
    }

    public BulletEntity(double x, double y, DirectionType direction, TankEntity owner) {
        super(x, y, 4, direction);
        this.owner = owner;
        setDuration(0.150);
    }

    public void move() {
        move(getDirection());
    }

    /*@Override
    public void move(DirectionType direction) {
        Objects.requireNonNull(direction, "Direction cannot be null");

        setState(StateType.MOVING);
        setDirection(direction);

        switch (direction) {
            case UP:
                setPoint(getPoint().add(0, -MOVING_STEP));
                break;
            case RIGHT:
                setPoint(getPoint().add(MOVING_STEP, 0));
                break;
            case DOWN:
                setPoint(getPoint().add(0, MOVING_STEP));
                break;
            case LEFT:
                setPoint(getPoint().add(-MOVING_STEP, 0));
                break;
        }

    }*/

    @Override
    public void render(GraphicsContext gc, double time) {

        /*gc.setFill(Color.WHITE);
        gc.fillRect(getRectangle().getMinX(), getRectangle().getMinY(), getRectangle().getWidth(), getRectangle().getHeight());*/

        if(isDestroy()) {
            if(isDestroyFinished()) {
                gc.drawImage(this.getExplosionImage(time), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
            } else {
                destroyed();
            }
        } else {
            double x = getPoint().getX() + ((BLOCK_SIDE_LENGTH - 8.0) / 2.0);
            double y = getPoint().getY() + ((BLOCK_SIDE_LENGTH - 8.0) / 2.0);
            switch (getDirection()) {
                case UP:
                    y = getPoint().getY();
                    break;
                case LEFT:
                    x = getPoint().getX();
                    break;
                case DOWN:
                    y = getPoint().getY() + (BLOCK_SIDE_LENGTH - 8.0);
                    break;
                case RIGHT:
                    x = getPoint().getX() + (BLOCK_SIDE_LENGTH - 8.0);
                    break;
            }
            gc.drawImage(this.resolveImageByDirection(time), x, y, BLOCK_SIDE_LENGTH / 4.0, BLOCK_SIDE_LENGTH / 4.0);
        }

    }

    @Override
    protected Image resolveImageByDirection(double time) {
        switch (getDirection()) {
            case UP:
                return BulletImageType.BULLET_UP.getImage();
            case RIGHT:
                return BulletImageType.BULLET_RIGHT.getImage();
            case DOWN:
                return BulletImageType.BULLET_DOWN.getImage();
            case LEFT:
                return BulletImageType.BULLET_LEFT.getImage();
        }
        return null;
    }

    private Image getExplosionImage(double time) {
        this.explosionImageCount++;
        return EXPLOSION[(int) ((time % (EXPLOSION.length * getDuration())) / getDuration())];
    }

    private boolean isDestroyFinished() {
        return this.explosionImageCount < EXPLOSION.length;
    }

}
