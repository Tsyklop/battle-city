package info.tsyklop.tanks.world.entity.impl.structure.spawn;

import info.tsyklop.tanks.type.LevelStructureMarkerType;
import info.tsyklop.tanks.world.entity.StructureEntity;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

public class PlayerSpawnEntity extends StructureEntity {

    public PlayerSpawnEntity(double x, double y, LevelStructureMarkerType index) {
        super(x, y, index);
    }

    @Override
    public boolean isStructure() {
        return false;
    }

}
