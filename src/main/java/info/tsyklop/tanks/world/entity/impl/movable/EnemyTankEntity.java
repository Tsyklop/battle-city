package info.tsyklop.tanks.world.entity.impl.movable;

import javafx.scene.image.Image;

public class EnemyTankEntity extends TankEntity {

    protected EnemyTankEntity(double x, double y) {
        super(x, y);
    }

    @Override
    public void upgrade() {

    }

    @Override
    protected Image resolveImageByDirection(final double time) {
        return null;
    }

    @Override
    public boolean isPlayer() {
        return false;
    }

}
