package info.tsyklop.tanks.world.entity;

import info.tsyklop.tanks.type.LevelStructureMarkerType;
import info.tsyklop.tanks.type.StructureImageType;
import info.tsyklop.tanks.world.entity.impl.movable.BulletEntity;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Arrays;

import static info.tsyklop.tanks.constant.Constant.*;

public abstract class StructureEntity extends WorldEntity {

    private final int[][] structure;

    private final double xPlusBlockSideHalfLength;
    private final double yPlusBlockSideHalfLength;

    private final StructureImageType structureImage;
    private final LevelStructureMarkerType markerType;

    public StructureEntity(double x, double y, LevelStructureMarkerType markerType) {
        this(x, y, markerType, null);
    }

    public StructureEntity(double x, double y, LevelStructureMarkerType markerType, StructureImageType structureImage) {
        super(x, y);
        this.markerType = markerType;
        this.structure = markerType.getStructure();
        this.structureImage = structureImage;
        this.xPlusBlockSideHalfLength = getPoint().getX() + BLOCK_SIDE_HALF_LENGTH;
        this.yPlusBlockSideHalfLength = getPoint().getY() + BLOCK_SIDE_HALF_LENGTH;
        createRectangle(markerType.getIndex());
    }

    public LevelStructureMarkerType getMarkerType() {
        return markerType;
    }

    public boolean isBonus() {
        return false;
    }

    public boolean isStructure() {
        return true;
    }

    public boolean isDestructible() {
        return true;
    }

    public void destruction(BulletEntity bullet) {
        if (isDestructible()) {

            for (int i = 0; i < this.structure.length; i++) {
                for (int j = 0; j < this.structure[i].length; j++) {
                    if (this.structure[i][j] == 1) {

                    }
                }
            }

            switch (bullet.getDirection()) {
                case UP:
                    switch (this.markerType.getIndex()) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            if (getRectangle().getMinX() == bullet.getRectangle().getMinX()) {
                                this.structure[0][2] = 0;
                                this.structure[0][3] = 0;
                                this.structure[1][2] = 0;
                                this.structure[1][3] = 0;
                            } else if (getRectangle().getMinX() >= bullet.getRectangle().getMinX()) {
                                this.structure[0][2] = 0;
                                this.structure[0][3] = 0;
                            } else if ((getRectangle().getMinX() + (getRectangle().getWidth() / 2)) <= bullet.getRectangle().getMinX()) {
                                this.structure[1][2] = 0;
                                this.structure[1][3] = 0;
                            }
                            break;
                        case 4:
                            break;
                        case 5:
                            break;
                        case 6:
                            break;
                        case 7:
                            break;
                        case 8:
                            break;
                    }

                    break;
                case LEFT:

                    break;
                case DOWN:

                    break;
                case RIGHT:

                    break;
            }

        }
    }

    @Override
    public void render(GraphicsContext gc, final double time) {

        renderByIndex(gc);

        /*if (this.markerType == LevelStructureMarkerType.B7 || this.markerType == LevelStructureMarkerType.B8) {
            gc.setFill(Color.GREEN);
            gc.fillRect(this.xPlusBlockSideHalfLength, this.yPlusBlockSideHalfLength, getRectangle().getWidth(), getRectangle().getHeight());
        }*/

        gc.setFill(Color.BLACK);

    }

    protected void renderByIndex(GraphicsContext gc) {

        for (int i = 0; i < this.structure.length; i++) {
            int block = i + 1;
            for (int j = 0; j < this.structure[i].length; j++) {
                if (this.structure[i][j] == 1) {

                    int blockElement = j + 1;
                    gc.drawImage(this.structureImage.getParts()[j], calculateXCoordinate(block, blockElement), calculateYCoordinate(block, blockElement));

                    /*if (this.markerType == LevelStructureMarkerType.B7 || this.markerType == LevelStructureMarkerType.B8) {
                        gc.setFill(Color.GREEN);
                        gc.fillRect(getRectangle().getMinX(), getRectangle().getMinY(), getRectangle().getWidth(), getRectangle().getHeight());
                    }*/

                }
            }
        }

        /*switch (this.index) {
            case 0:
                gc.drawImage(structureImage.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                gc.drawImage(structureImage.getImage(), xPlusBlockSideHalfLength, getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                gc.drawImage(structureImage.getImage(), getPoint().getX(), yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                gc.drawImage(structureImage.getImage(), xPlusBlockSideHalfLength, yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 1:
                gc.drawImage(structureImage.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                gc.drawImage(structureImage.getImage(), getPoint().getX(), yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 2:
                gc.drawImage(structureImage.getImage(), xPlusBlockSideHalfLength, getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                gc.drawImage(structureImage.getImage(), xPlusBlockSideHalfLength, yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 3:
                gc.drawImage(structureImage.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                gc.drawImage(structureImage.getImage(), xPlusBlockSideHalfLength, getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 4:
                gc.drawImage(structureImage.getImage(), getPoint().getX(), yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                gc.drawImage(structureImage.getImage(), xPlusBlockSideHalfLength, yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 5:
                gc.drawImage(structureImage.getImage(), getPoint().getX(), getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 6:
                gc.drawImage(structureImage.getImage(), xPlusBlockSideHalfLength, getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 7:
                gc.drawImage(structureImage.getImage(), xPlusBlockSideHalfLength, yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 8:
                gc.drawImage(structureImage.getImage(), getPoint().getX(), yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
        }*/

    }

    private void createRectangle(int index) {
        switch (index) {
            case 0:
                setRectangle(getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
                break;
            case 1:
                setRectangle(getPoint().getX(), getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_LENGTH);
                break;
            case 2:
                setRectangle(xPlusBlockSideHalfLength, getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 3:
                setRectangle(getPoint().getX(), getPoint().getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 4:
                setRectangle(getPoint().getX(), yPlusBlockSideHalfLength, BLOCK_SIDE_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 5:
                setRectangle(getPoint().getX(), getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 6:
                setRectangle(xPlusBlockSideHalfLength, getPoint().getY(), BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 7:
                setRectangle(getPoint().getX(), yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
            case 8:
                setRectangle(xPlusBlockSideHalfLength, yPlusBlockSideHalfLength, BLOCK_SIDE_HALF_LENGTH, BLOCK_SIDE_HALF_LENGTH);
                break;
        }
    }

    private double calculateXCoordinate(int block, int blockElement) {
        switch (block) {
            case 1:
            case 3:
                switch (blockElement) {
                    case 1:
                    case 3:
                        return getPoint().getX();
                    case 2:
                    case 4:
                        return this.xPlusBlockSideHalfLength - BLOCK_SIDE_HALF_HALF_LENGTH;
                }
                break;
            case 2:
            case 4:
                switch (blockElement) {
                    case 1:
                    case 3:
                        return this.xPlusBlockSideHalfLength;
                    case 2:
                    case 4:
                        return this.xPlusBlockSideHalfLength + BLOCK_SIDE_HALF_HALF_LENGTH;
                }
                break;
        }
        return getPoint().getX();
    }

    private double calculateYCoordinate(int block, int blockElement) {
        switch (block) {
            case 1:
            case 2:
                switch (blockElement) {
                    case 1:
                    case 2:
                        return getPoint().getY();
                    case 3:
                    case 4:
                        return this.yPlusBlockSideHalfLength - BLOCK_SIDE_HALF_HALF_LENGTH;
                }
                break;
            case 3:
            case 4:
                switch (blockElement) {
                    case 1:
                    case 2:
                        return this.yPlusBlockSideHalfLength;
                    case 3:
                    case 4:
                        return this.yPlusBlockSideHalfLength + BLOCK_SIDE_HALF_HALF_LENGTH;
                }
                break;
        }
        return getPoint().getY();
    }

}
