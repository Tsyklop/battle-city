package info.tsyklop.tanks.world.entity;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

import java.util.Objects;

import static info.tsyklop.tanks.constant.Constant.BLOCK_SIDE_LENGTH;

public abstract class WorldEntity implements GraphicEntity {

    private Point2D point;

    private Rectangle2D rectangle;

    public WorldEntity(double x, double y) {
        setPoint(new Point2D(x, y));
    }

    public Point2D getPoint() {
        return point;
    }

    public void setPoint(Point2D point) {
        Objects.requireNonNull(point, "Point cannot be null");
        this.point = point;
        setRectangle(this.point.getX(), this.point.getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
    }

    public Rectangle2D getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle2D rectangle) {
        this.rectangle = rectangle;
    }

    public void setRectangle(double x, double y, double width, double height) {
        setRectangle(new Rectangle2D(x, y, width, height));
    }

    /*protected void resetRectangle() {
        if(this.point != null) {
            createRectangle(this.point.getX(), this.point.getY(), BLOCK_SIDE_LENGTH, BLOCK_SIDE_LENGTH);
        }
    }

    protected void createRectangle(double x, double y, double width, double height) {
        this.rectangle = new Rectangle2D(x, y, width, height);
    }*/

}
