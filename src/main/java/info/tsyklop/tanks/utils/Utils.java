package info.tsyklop.tanks.utils;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.image.Image;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Utils {

   public static Image loadImage(String path) {
       return new Image(getResourceAsStream(path));
   }

    public static List<String> getResourceFiles(String path) throws IOException {
        return getResourceFiles(path, null);
    }

    public static List<String> getResourceFiles(String path, String extension) throws IOException {
        List<String> filenames = new ArrayList<>();

        try (InputStream in = getResourceAsStream(path);
             BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            String resource;
            while ((resource = br.readLine()) != null) {
                if(extension == null) {
                    filenames.add(resource);
                } else if (resource.endsWith(extension)) {
                    filenames.add(resource);
                }
            }
        }

        return filenames;
    }

    public static URL getResourceURL(String path) {
        return Utils.class.getClassLoader().getResource(path);
    }

    public static InputStream getResourceAsStream(String path) {
        return Utils.class.getClassLoader().getResourceAsStream(path);
    }

    public static String getResourceToString(String path) throws IOException {
        return IOUtils.toString(getResourceAsStream(path), StandardCharsets.UTF_8);
    }

    public static void exit() {
        Platform.exit();
        System.exit(0);
    }

    public static void runThread(Runnable runnable) {
        Platform.runLater(runnable);
    }

    public static void runDaemonThread(Task task) {
        Thread th = new Thread(task);
        th.setDaemon(false);
        th.start();
    }

}
