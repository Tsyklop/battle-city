package info.tsyklop.tanks.controller;

import info.tsyklop.tanks.game.Game;
import info.tsyklop.tanks.game.GameLoop;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import static info.tsyklop.tanks.constant.Constant.*;

public class WindowController extends BaseController {

    @FXML
    private Canvas canvas;

    @FXML
    private Button controlButton;

    private final Game game = Game.getInstance();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.canvas.setWidth(GAME_FIELD_SIDE_LENGTH * BLOCK_SIDE_LENGTH);
        this.canvas.setHeight(GAME_FIELD_SIDE_LENGTH * BLOCK_SIDE_LENGTH);
        this.controlButton.setFocusTraversable(false);
    }

    @Override
    public void onControllerLoaded() { }

    @FXML
    void onControlHandle(MouseEvent event) {

        if(this.game.isStarted()) {
            this.game.stop();
            this.controlButton.setText("Start");
            getStage().getScene().removeEventHandler(KeyEvent.KEY_PRESSED, getStage().getScene().getOnKeyPressed());
            getStage().getScene().removeEventHandler(KeyEvent.KEY_RELEASED, getStage().getScene().getOnKeyReleased());
        } else {

            try {
                if(this.game.start(this.canvas.getGraphicsContext2D())) {
                    getStage().getScene().setOnKeyPressed(ev ->  this.game.getWorld().addKey(ev.getCode()));
                    getStage().getScene().setOnKeyReleased(ev -> this.game.getWorld().removeKey(ev.getCode()));
                    this.controlButton.setText("Stop");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            /*if(this.world.loadNextLevel()) {

                this.started = true;
                this.controlButton.setText("Stop");

                getStage().getScene().setOnKeyPressed(ev ->  this.world.addKey(ev.getCode()));
                getStage().getScene().setOnKeyReleased(ev -> this.world.removeKey(ev.getCode()));

            }*/

        }

    }

}
