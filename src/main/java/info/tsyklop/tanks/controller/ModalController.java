package info.tsyklop.tanks.controller;

import javafx.stage.Stage;

import java.util.Map;

public abstract class ModalController extends BaseController {

    protected Map<String, Object> data;

    public abstract void modalInitialized();

    public void setData(Map<String, Object> data) {
        if(this.data != null) {
            throw new IllegalArgumentException("Cannot replace data");
        }
        this.data = data;
    }

}
