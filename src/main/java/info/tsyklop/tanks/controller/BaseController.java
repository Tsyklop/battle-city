package info.tsyklop.tanks.controller;

import info.tsyklop.tanks.window.Window;
import info.tsyklop.tanks.world.World;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

public abstract class BaseController implements Initializable {

    private Stage stage;

    protected final Window window = Window.getInstance();

    public abstract void onControllerLoaded();

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        if(this.stage != null) {
            throw new IllegalArgumentException("Cannot replace stage");
        }
        this.stage = stage;
    }

    protected void closeStage() {
        if(this.stage != null) {
            this.stage.close();
        }
    }

}
