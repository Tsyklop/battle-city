package info.tsyklop.tanks;

import info.tsyklop.tanks.window.Window;
import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tanks extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Tanks.class);

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Window window = Window.getInstance(primaryStage);
        window.show("window");
    }

}
